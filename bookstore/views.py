from django.views.generic.base import TemplateView

from books.models import Category, Book


class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        slug = kwargs.get('slug', None)
        context_data = super(IndexView, self).get_context_data(**kwargs)
        context_data["categories"] = Category.objects.all()
        context_data["category"] = None
        books = Book.objects.filter(cover__isnull=False)
        if slug:
            category = Category.objects.get(slug=slug)
            books = books.filter(category=category)
            context_data["category"] = category
        books = books.order_by("?")[:9]
        context_data["books"] = books
        return context_data
