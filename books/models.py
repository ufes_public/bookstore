from datetime import date

from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from books import isbn


class Author(models.Model):
    """Author model."""
    GENDER_CHOICES = (
        (1, 'Male'),
        (2, 'Female'),
    )

    first_name = models.CharField(_('first name'), blank=True, max_length=100)
    middle_name = models.CharField(_('middle name'), blank=True, max_length=100)
    last_name = models.CharField(_('last name'), blank=True, max_length=100)
    gender = models.PositiveSmallIntegerField(_('gender'), choices=GENDER_CHOICES, blank=True, null=True)
    birth_date = models.DateField(_('birth date'), blank=True, null=True)
    website = models.URLField(_('website'), blank=True)

    class Meta:
        verbose_name = _('author')
        verbose_name_plural = _('authors')
        ordering = ('last_name', 'first_name',)

    def __str__(self):
        return u'%s' % self.full_name

    @property
    def full_name(self):
        return u'%s %s' % (self.first_name, self.last_name)

    @property
    def age(self):
        age = None
        if self.birth_date:
            today = date.today()
            age = today.year - self.birth_date.year - ((today.month, today.day) < (self.birth_date.month, self.birth_date.day))
        return age


class Category(models.Model):
    """Category model"""
    title = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        return super(Category, self).save(*args, **kwargs)

    class Meta:
        ordering = ('title',)
        verbose_name = _("category")
        verbose_name_plural = _("categories")

    def __str__(self):
        return '%s' % self.title


class Book(models.Model):
    """Listing of books"""
    title = models.CharField(max_length=255)
    prefix = models.CharField(max_length=20, blank=True)
    subtitle = models.CharField(blank=True, max_length=255)
    slug = models.SlugField(unique=True)
    authors = models.ManyToManyField(Author, related_name='books')
    isbn = models.CharField(max_length=14, blank=True, validators=[isbn.django_validator])
    pages = models.PositiveSmallIntegerField(blank=True, null=True, default=0)
    published = models.DateField(blank=True, null=True)
    cover = models.ImageField(upload_to='books', blank=True)
    description = models.TextField(blank=True)
    category = models.ManyToManyField(Category, blank=True, related_name="books")
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = "%s-%s" % (slugify(self.title), self.isbn)
        return super(Book, self).save(*args, **kwargs)

    class Meta:
        ordering = ('title',)

    def __str__(self):
        return '%s' % self.full_title

    @property
    def full_title(self):
        if self.prefix:
            return '%s %s' % (self.prefix, self.title)
        else:
            return '%s' % self.title

    def get_url(self, type=None):
        return isbn.url(self.isbn, type)
