from django.contrib import admin

from books.models import Author, Category, Book


class AuthorAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name')
admin.site.register(Author, AuthorAdmin)


class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
admin.site.register(Category, CategoryAdmin)


class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'pages')
    prepopulated_fields = {'slug': ('title',)}
admin.site.register(Book, BookAdmin)

admin.site.site_header = 'Bookstore - Administration'
