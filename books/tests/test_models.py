import datetime

from django.test import TestCase as DjangoTestCase

from books.models import Author, Category, Book
from django.core.exceptions import ValidationError


class AuthorTestCase(DjangoTestCase):

    def test_author_full_name(self):
        '''
        Test author full name
        '''
        author = Author()
        author.first_name = "First"
        author.last_name = "Last"
        self.assertEqual(author.full_name, "First Last")

    def test_author_age(self):
        '''
        Test author age
        '''
        author = Author()
        today = datetime.date.today()
        author.birth_date = datetime.datetime(today.year - 20, today.month, today.day)
        self.assertEqual(author.age, 20)

    def test_author_age_none_birth_date(self):
        '''
        Test author age
        '''
        author = Author()
        author.birth_date = None
        self.assertEqual(author.age, None)

    def test_cagtegory_slug(self):
        '''
        Test category slug
        '''
        category = Category()
        category.title = "Category Title"
        category.save()
        self.assertEqual(category.slug, "category-title")


class BookTestCase(DjangoTestCase):

    def test_book_isbn(self):
        book = Book(price=39, slug="test", title="Teste")
        book.isbn = "9781584885405"
        book.full_clean()

    def test_book_wrong_isbn(self):
        book = Book(price=39, slug="test", title="Teste")
        book.isbn = "9781584885404"
        with self.assertRaises(ValidationError):
            book.full_clean()

    def test_url(self):
        '''
        Test URL generation
        '''
        book = Book(isbn="0375508732")
        self.assertEqual(book.get_url(), "http://books.google.com/books?vid=0375508732")
