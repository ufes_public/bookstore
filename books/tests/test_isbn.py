from django.test import TestCase as DjangoTestCase

from books import isbn


class ISBNTestCase(DjangoTestCase):

    def test_isbn_strip(self):
        self.assertEqual(isbn.isbn_strip("978-158488-540-5"), "9781584885405")

    def test_isbn_digit(self):
        self.assertEqual(isbn.check("1-58488-540"), "8")
        self.assertNotEqual(isbn.check("978-158488-540-5"), "8")
