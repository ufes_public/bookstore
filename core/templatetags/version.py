from django import template
import bookstore

register = template.Library()


@register.simple_tag
def version():
    return bookstore.__version__
