from django.test import Client
from django.test import TestCase as DjangoTestCase
from django.urls import reverse


class AdminLoginRoutesTestCase(DjangoTestCase):

    fixtures = ['auth.json', ]

    def test_login(self):
        response = self.client.get(reverse('admin:login'))
        self.assertContains(response, 'Log in')

    def test_user_login(self):
        c = Client()
        response = c.post(reverse("admin:login"), 
                          {'username': 'admin', 'password': 'senha@1234'})
        self.assertEquals(response.status_code, 302)

    def test_wrong_login(self):
        c = Client()
        response = c.post(reverse("admin:login"), 
                          {'username': 'teste', 'password': '123'})
        self.assertContains(response, 
                            "Please enter the correct username and password")
