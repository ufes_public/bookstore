import os
import zipfile

from django.conf import settings
from django.contrib.auth.models import User
from django.core.management import call_command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Loads demo data'

    def handle(self, *args, **options):
        if not User.objects.all().exists():
            call_command('loaddata', 'auth.json', app_label='core')
            call_command('loaddata', 'authors.json', app_label='books')
            call_command('loaddata', 'categories.json', app_label='books')
            call_command('loaddata', 'books.json', app_label='books')

            with zipfile.ZipFile(os.path.join(settings.BASE_DIR, 'books', 'fixtures', 'books.zip'), "r") as zip_ref:
                zip_ref.extractall(settings.MEDIA_ROOT)
