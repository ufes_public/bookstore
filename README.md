[![build status](https://gitlab.com/ufes_public/bookstore/badges/master/build.svg)](https://gitlab.com/ufes_public/bookstore/commits/master)
[![coverage report](https://gitlab.com/ufes_public/bookstore/badges/master/coverage.svg)](https://gitlab.com/ufes_public/bookstore/commits/master)
<br /><br />

![bookstore logo](https://gitlab.com/ufes_public/bookstore/raw/master/bookstore/static/img/bookstore_logo.png)

LongStory - Loja de Livros para demonstração de testes integrados com Gitlab.

## Instalação

## 1. Clonar o projeto

	git clone https://gitlab.com/ufes_public/bookstore.git

## 2. Criar ambiente virtual

    python3 -m venv /path/to/env

## 3. Instalar dependências

    source /path/to/env/bin/activate
    pip3 install -r requirements.dev.pip

## 4. Alterar configurações

Alterar configurações do banco no arquivo de configurações (bookstore/settings.py)

## 5. Criar banco e sincronizar dados

    ./manage.py migrate

## 6. Carregar dados de exemplo

O comando abaixo cria dados de demonstração:

    ./manage.py demo
    
## 7. Acessar o sistema

Abra o browser e acesse http://localhost:8000/

usuário: admin
senha: senha@1234

<h2 align="center">Links Importantes</h2>

- [Demo](https://bookstore.sloppy.zone/)