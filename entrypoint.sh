#!/bin/bash
set -e

[[ $DEBUG == true ]] && set -x

case ${1} in
  	app:init|app:start|app:command|app:shell)
		python ${BOOKSTORE_HOME}/manage.py migrate
		python ${BOOKSTORE_HOME}/manage.py demo
	
    case ${1} in
      app:start)
      	python ${BOOKSTORE_HOME}/manage.py runserver 0.0.0.0:8000
        ;;
      app:command)
        shift 1
        python ${BOOKSTORE_HOME}/manage.py $@
        ;;
      app:shell)
        ;;
  esac
    ;;
  app:help)
    echo "Available options:"
    echo " app:start              - Starts the Salus Server (default)"
    echo " app:init               - Initialize the Salus Server (e.g. create databases, compile assets), but don't start it."
    echo " app:help               - Displays the help"
    echo " app:command <command>  - Execute a Django command."
    echo " [command]              - Execute the specified command, eg. bash."
    ;;
  *)
    exec "$@"
    ;;
esac
