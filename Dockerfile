FROM python:3.5.3-alpine
ENV DEBIAN_FRONTEND=noninteractive \
    LANG=C.UTF-8
ENV BOOKSTORE_HOME="/opt/bookstore" 

RUN apk --no-cache add build-base jpeg-dev zlib-dev bash

COPY . ${BOOKSTORE_HOME}
WORKDIR ${BOOKSTORE_HOME}
RUN pip install -r requirements.pip

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["app:start"]